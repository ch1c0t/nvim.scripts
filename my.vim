inoremap jk <esc>

imap <C-b> ``<esc>i

filetype plugin indent on
set tabstop=2
set shiftwidth=2
set expandtab

set number
set rnu

syntax on
colorscheme onedark

nmap <C-a> :w<CR>:so %<CR>

" https://superuser.com/questions/693528/vim-is-there-a-downside-to-using-space-as-your-leader-key/693644#693644
map <space> \

nmap <leader>f :GFiles<CR>
nmap <leader>F :Files<CR>
nmap <leader>b :Buffers<CR>
nmap <leader>h :History<CR>
nmap <leader>t :BTags<CR>
nmap <leader>T :Tags<CR>
nmap <leader>l :BLines<CR>
nmap <leader>L :Lines<CR>

" https://vi.stackexchange.com/questions/22779/how-to-open-files-in-vertical-splits-by-default
"augroup some
	"au!
	"au WinNew * wincmd L
"augroup END

nmap <leader>d odbg!(<esc>A;<esc>hi
