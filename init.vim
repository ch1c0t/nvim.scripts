let g:nvim_config_root = stdpath('config')
let g:config_file_list = ['plugins.vim', 'my.vim']

for file in g:config_file_list
	exec 'source ' . g:nvim_config_root . '/' . file
endfor
